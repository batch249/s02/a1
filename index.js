//S2 QUIZ

/*
1.) Spaghetti Code
2.) {}
3.) encapsulation
4.) studentOne.enroll();
5.) true
6.) key:value
7.) true
8.) true
9.) true
10.) true
*/


//ACTIVITY FUNCTION CODING
//1, 2, 3, 4 -----------------------
let studentOne = {
	name: 'John',
	email: 'john@mail.com',
	grades: [89, 84, 78, 88],
	
	login() {
		console.log(`${this.email} has logged in`);
	},

	logout() {
		console.log(`${this.email} has logged out`);
	},

	computeAve() {
		let ave = 0;
		this.grades.forEach(grade => ave += grade);
		return ave / 4;
	},

	willPass() {
		return this.computeAve() >= 85 ? true : false;
	},

	willPassWithHonors() {
		return this.willPass() == true && this.computeAve >= 90 ? true : undefined
	}
}

let studentTwo = {
	name: 'Joe',
	email: 'joe@mail.com',
	grades: [78, 82, 79, 85],
	
	login() {
		console.log(`${this.email} has logged in`);
	},

	logout() {
		console.log(`${this.email} has logged out`);
	},

	computeAve() {
		let ave = 0;
		this.grades.forEach(grade => ave += grade);
		return ave / 4;
	},

	willPass() {
		return this.computeAve() >= 85 ? true : false;
	},

	willPassWithHonors() {
		return this.willPass() == true && this.computeAve >= 90 ? true : undefined
	}
}


let studentThree = {
	name: 'Jane',
	email: 'jane@mail.com',
	grades: [87, 89, 91, 93],
	
	login() {
		console.log(`${this.email} has logged in`);
	},

	logout() {
		console.log(`${this.email} has logged out`);
	},

	computeAve() {
		let ave = 0;
		this.grades.forEach(grade => ave += grade);
		return ave / 4;
	},

	willPass() {
		return this.computeAve() >= 85 ? true : false;
	},

	willPassWithHonors() {
		return this.willPass() == true && this.computeAve >= 90 ? true : undefined
	}
}


let studentFour = {
	name: 'Jessie',
	email: 'jessie@mail.com',
	grades: [91, 89, 92, 93],
	
	login() {
		console.log(`${this.email} has logged in`);
	},

	logout() {
		console.log(`${this.email} has logged out`);
	},

	computeAve() {
		let ave = 0;
		this.grades.forEach(grade => ave += grade);
		return ave / 4;
	},

	willPass() {
		return this.computeAve() >= 85 ? true : false;
	},

	willPassWithHonors() {
		return this.willPass() == true && this.computeAve >= 90 ? true : undefined
	}
}

//5, 6  -----------------------
let classOf1A = {
	students : [
	{
		name: 'John',
		email: 'john@mail.com',
		grades: [89, 84, 78, 88],
		
		login() {
			console.log(`${this.email} has logged in`);
		},

		logout() {
			console.log(`${this.email} has logged out`);
		},

		computeAve() {
			let ave = 0;
			this.grades.forEach(grade => ave += grade);
			return ave / 4;
		},

		willPass() {
			return this.computeAve() >= 85 ? true : false;
		},

		willPassWithHonors() {
			return this.willPass() == true && this.computeAve >= 90 ? true : undefined
		}
	},

	{
		name: 'Joe',
		email: 'joe@mail.com',
		grades: [78, 82, 79, 85],
		
		login() {
			console.log(`${this.email} has logged in`);
		},

		logout() {
			console.log(`${this.email} has logged out`);
		},

		computeAve() {
			let ave = 0;
			this.grades.forEach(grade => ave += grade);
			return ave / 4;
		},

		willPass() {
			return this.computeAve() >= 85 ? true : false;
		},

		willPassWithHonors() {
			return this.willPass() == true && this.computeAve >= 90 ? true : undefined
		}
	},

	{
		name: 'Jane',
		email: 'jane@mail.com',
		grades: [87, 89, 91, 93],
		
		login() {
			console.log(`${this.email} has logged in`);
		},

		logout() {
			console.log(`${this.email} has logged out`);
		},

		computeAve() {
			let ave = 0;
			this.grades.forEach(grade => ave += grade);
			return ave / 4;
		},

		willPass() {
			return this.computeAve() >= 85 ? true : false;
		},

		willPassWithHonors() {
			return this.willPass() == true && this.computeAve >= 90 ? true : undefined
		}
	},

	{
		name: 'Jessie',
		email: 'jessie@mail.com',
		grades: [91, 89, 92, 93],
		
		login() {
			console.log(`${this.email} has logged in`);
		},

		logout() {
			console.log(`${this.email} has logged out`);
		},

		computeAve() {
			let ave = 0;
			this.grades.forEach(grade => ave += grade);
			return ave / 4;
		},

		willPass() {
			return this.computeAve() >= 85 ? true : false;
		},

		willPassWithHonors() {
			return this.willPass() == true && this.computeAve >= 90 ? true : undefined
		}
	}

	],
	countHonorStudents() {
		let honorCount = 0;
		this.students.forEach(object => object.willPass == true ? honorCount+1 : honorCount = honorCount)
		return  honorCount;
	},

	honorsPercentage() {
		let percentage = (this.countHonorStudents() / this.students.length) * 100;
		return percentage;
	}
}

//7 -----------------------
//8 -----------------------
//9 -----------------------